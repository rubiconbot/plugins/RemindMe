/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.remindme.entity;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.Table;
import com.datastax.driver.mapping.annotations.Transient;
import fun.rubicon.plugin.io.db.Cassandra;
import fun.rubicon.plugin.io.db.DatabaseEntity;
import fun.rubicon.plugin.util.SafeMessage;
import fun.rubicon.remindme.RemindPlugin;
import lombok.Getter;
import lombok.extern.log4j.Log4j;
import net.dv8tion.jda.core.entities.User;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Consumer;

@Table(name = "reminders")
@Log4j
public class Remind extends DatabaseEntity<Remind> {

    @Transient
    private Timer timer = new Timer();
    @Column(name="user_id")
    private long userId;
    @Transient
    @Getter
    private User user;
    @Getter
    private long expiry;
    @Column(name="remind_text")
    @Getter
    private String remindText;

    public Remind() {
        super(Remind.class, Cassandra.getCassandra());
    }

    public Remind(User user, long expiry, String remindText) {
        super(Remind.class, Cassandra.getCassandra());
        this.user = user;
        this.expiry = expiry;
        this.remindText = remindText;
    }

    public Remind init() {
        this.user = RemindPlugin.getInstance().getRubicon().getShardManager().getUserById(this.userId);
        this.schedule();
        return this;
    }

    private void schedule() {
        this.timer.schedule(new TimerTask() {
            @Override
            public void run() {
                SafeMessage.sendMessage(Remind.this.user.openPrivateChannel().complete(),String.format("You wanted to be reminded to do the following: ```%s```",Remind.this.remindText));
            }
        }, new Date(expiry));
    }

    public void close() {
        this.timer.cancel();
        this.delete(remind -> {
                    log.debug(String.format("[DB] Deleted reminder for user %s", this.userId));
                }
        );
    }

    @Override
    public void save() {
        save(this, null, null);
    }

    @Override
    public void save(Consumer<Remind> onSuccess) {
        save(this, onSuccess, null);
    }

    @Override
    public void save(Consumer<Remind> onSuccess, Consumer<Throwable> onError) {
        save(this, onSuccess, onError);
    }

    @Override
    public void delete() {
        delete(this, null, null);
    }

    @Override
    public void delete(Consumer<Remind> onSuccess) {
        delete(this, onSuccess, null);
    }

    @Override
    public void delete(Consumer<Remind> onSuccess, Consumer<Throwable> onError) {
        delete(this, onSuccess, onError);
    }

    private String formateException(Throwable t) {
        StringWriter writer = new StringWriter();
        PrintWriter pr = new PrintWriter(writer);
        t.printStackTrace(pr);
        return writer.getBuffer().toString();
    }
}


