/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.remindme;

import fun.rubicon.remindme.entity.Remind;
import java.util.HashMap;
import net.dv8tion.jda.core.entities.User;

public class RemindManager
{
    public HashMap<User, Remind> getRemindCache() { return remindCache; } private HashMap<User, Remind> remindCache = new HashMap();

    public Remind getRemindByUserId(User user)
    {
        return (Remind)remindCache.get(user);
    }

    public RemindManager() {}
}
