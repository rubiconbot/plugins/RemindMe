/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.remindme;

import fun.rubicon.plugin.command.Command;
import fun.rubicon.plugin.command.CommandCategory;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;
import fun.rubicon.remindme.entity.Remind;

public class CommandCancelRemind extends Command {

    public CommandCancelRemind() {
        super(new String[]{"cancel"}, CommandCategory.UTILITY, "", "Cancel your Reminder");
    }

    public Result execute(CommandEvent event, String[] args) {
        RemindManager manager = RemindPlugin.getInstance().getRemindManager();
        Remind remind = manager.getRemindByUserId(event.getAuthor());
        if (remind == null)
            return send(error("No reminder", "You haven't set a reminder, yet."));
        remind.close();
        return send(success("Reminder deleted", "Your reminder was successfully deleted."));
    }
}