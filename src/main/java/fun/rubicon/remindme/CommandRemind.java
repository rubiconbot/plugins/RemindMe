/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.remindme;

import fun.rubicon.plugin.command.Command;
import fun.rubicon.plugin.command.CommandCategory;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;
import fun.rubicon.plugin.util.StringUtil;
import fun.rubicon.remindme.entity.Remind;

public class CommandRemind extends Command {
    public CommandRemind() {
        super(new String[]{"remind", "remindme"}, CommandCategory.UTILITY, "create <m/d/w/M> <your remind> - you will get reminded in <minutes/days/weeks/months> days", "Get reminded after given period of time");
        this.registerSubCommand(new CommandCancelRemind());
    }

    public Result execute(CommandEvent event, String[] args) {
        if (args.length < 2) {
            return this.sendHelp(event);
        }
        if (!args[0].equalsIgnoreCase("create")) {
            return this.sendHelp(event);
        }
        Remind remind = new Remind(event.getAuthor(), StringUtil.parseDate(args[1]).getTime(), event.getArgsAsString().replace(args[0], "").replace(args[1], ""));
        return this.send(CommandRemind.success("Success", String.format("You will be Reminded in %s", args[1])));
    }
}
