/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.remindme;

import fun.rubicon.plugin.Plugin;
import fun.rubicon.plugin.io.db.Cassandra;
import fun.rubicon.remindme.entity.RemindAccessor;

public class RemindPlugin extends Plugin {
    private RemindManager remindManager;
    private static RemindPlugin instance;

    public void init() {
        instance = this;
        this.remindManager = new RemindManager();
    }

    public void onEnable() {
        this.createRemindsTable();
        this.loadCache();
        this.getRubicon().registerCommand(this, new CommandRemind());
    }

    private void loadCache() {
        Cassandra.getCassandra().getMappingManager().createAccessor(RemindAccessor.class).getAllReminders().all().forEach(remind -> this.remindManager.getRemindCache().put(remind.init().getUser(), remind)
        );
    }

    private void createRemindsTable() {
        Cassandra.getCassandra().getConnection().execute("CREATE TABLE IF NOT EXISTS reminders (user_id BIGINT PRIMARY KEY,expiry BIGINT,remind_text TEXT)");
    }

    public RemindManager getRemindManager() {
        return this.remindManager;
    }

    public static RemindPlugin getInstance() {
        return instance;
    }
}
